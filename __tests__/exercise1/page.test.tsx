import React from "react";
import "@testing-library/jest-dom";
import { render, screen, waitFor } from "@testing-library/react";
import Exercise1 from "../../app/exercise1/page";

import "jest-canvas-mock";

describe("Exercise 1", () => {
  it("should render the component", () => {
    render(<Exercise1 />);

    expect(screen.getByText("Normal range")).toBeInTheDocument();
  });

  it("range should be fetched", async () => {
    const setState = jest.fn();
    jest
      .spyOn(React, "useState")
      .mockImplementationOnce(() => [undefined, setState]);

    global.fetch = jest
      .fn()
      .mockImplementationOnce(
        jest.fn(() =>
          Promise.resolve({ json: () => Promise.resolve({ min: 0, max: 10 }) })
        ) as jest.Mock
      );

    render(<Exercise1 />);

    expect(global.fetch).toHaveBeenCalledWith(
      "https://demo1553213.mockable.io/normal"
    );

    await waitFor(() => {
      expect(setState).toHaveBeenCalledWith({ min: 0, max: 10 });
    });
  });
});
