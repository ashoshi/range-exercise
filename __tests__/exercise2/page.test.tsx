import React from "react";
import "@testing-library/jest-dom";
import { render, screen, waitFor } from "@testing-library/react";
import Exercise2 from "../../app/exercise2/page";

import "jest-canvas-mock";

const rangeValues = { range: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10] };

describe("Exercise 2", () => {
  it("should render the component", () => {
    render(<Exercise2 />);

    expect(screen.getByText("Fixed range")).toBeInTheDocument();
  });

  it("range should be fetched", async () => {
    const setState = jest.fn();
    jest
      .spyOn(React, "useState")
      .mockImplementationOnce(() => [undefined, setState]);

    global.fetch = jest
      .fn()
      .mockImplementationOnce(
        jest.fn(() =>
          Promise.resolve({ json: () => Promise.resolve(rangeValues) })
        ) as jest.Mock
      );

    render(<Exercise2 />);

    expect(global.fetch).toHaveBeenCalledWith(
      "https://demo1553213.mockable.io/fixed"
    );

    await waitFor(() => {
      expect(setState).toHaveBeenCalledWith(rangeValues);
    });
  });
});
