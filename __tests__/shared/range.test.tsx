import React from "react";
import "@testing-library/jest-dom";
import {
  getAllByText,
  getByTestId,
  getByTitle,
  render,
  screen,
  waitFor,
} from "@testing-library/react";
import Range from "../../app/shared/components/range/range";

import "jest-canvas-mock";

describe("Range", () => {
  let normalRange = { min: 0, max: 20 };
  let fixedRange = [0, 10, 20, 30, 40];

  it("should render fine", () => {
    render(<Range values={normalRange} onChange={() => {}} />);
  });

  it("should render prefix in normal range", () => {
    let { getAllByText, getByTestId } = render(
      <Range values={normalRange} onChange={() => {}} prefix="$" />
    );
    expect(getByTestId("min-input")).toHaveDisplayValue("0");
    expect(getByTestId("max-input")).toHaveDisplayValue("20");
    expect(getAllByText("$", { selector: "span" }));
  });

  it("should render prefix in fixed range", () => {
    let { getAllByText, getByTestId } = render(
      <Range values={fixedRange} onChange={() => {}} prefix="$" />
    );
    expect(getByTestId("min-input")).toHaveDisplayValue("0");
    expect(getByTestId("max-input")).toHaveDisplayValue("40");
    expect(getAllByText("$", { selector: "span" }));
  });

  it("should render suffix in normal range", () => {
    let { getAllByText, getByTestId } = render(
      <Range values={normalRange} onChange={() => {}} suffix="/h" />
    );
    expect(getByTestId("min-input")).toHaveDisplayValue("0");
    expect(getByTestId("max-input")).toHaveDisplayValue("20");
    expect(getAllByText("/h", { selector: "span" }));
  });

  it("should render suffix in fixed range", () => {
    let { getAllByText, getByTestId } = render(
      <Range values={fixedRange} onChange={() => {}} suffix="/h" />
    );
    expect(getByTestId("min-input")).toHaveDisplayValue("0");
    expect(getByTestId("max-input")).toHaveDisplayValue("40");
    expect(getAllByText("/h", { selector: "span" }));
  });
});
