export async function GET() {
  const res = await fetch("https://demo1553213.mockable.io/range");
  const data = await res.json();

  return Response.json({ data });
}
