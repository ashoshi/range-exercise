import { NextResponse } from "next/server";

export async function GET() {
  const res = await fetch("https://demo1553213.mockable.io/normal");
  const data = await res.json();

  return NextResponse.json({ data });
}
