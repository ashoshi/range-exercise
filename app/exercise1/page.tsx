"use client";

import React, { useEffect, useState } from "react";
import Range from "../shared/components/range/range";
import { RangeType } from "../shared/types/range";

export default function Exercise1() {
  const [range, setRange] = useState<RangeType>();

  const fetchRange = async () => {
    try {
      const response = await fetch("https://demo1553213.mockable.io/normal");
      const data = await response.json();

      setRange(data);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    fetchRange();
  }, []);

  return (
    <div className="flex flex-col items-center container mt-4">
      <h1 className="text-2xl font-bold">Normal range</h1>
      {range !== undefined && (
        <Range
          values={range}
          prefix="$"
          onChange={(values) => console.log(values)}
        />
      )}
    </div>
  );
}
