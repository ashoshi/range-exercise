"use client";

import React, { useEffect, useState } from "react";
import Range from "../shared/components/range/range";

export default function Exercise2() {
  const [range, setRange] = useState<any>();

  const fetchRange = async () => {
    try {
      const response = await fetch("https://demo1553213.mockable.io/fixed");
      const data = await response.json();

      setRange(data);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    fetchRange();
  }, []);

  return (
    <div className="flex flex-col items-center container">
      <h1 className="text-2xl font-bold">Fixed range</h1>
      {range !== undefined && (
        <Range
          values={range.range}
          prefix="$"
          onChange={(values) => console.log(values)}
        />
      )}
    </div>
  );
}
