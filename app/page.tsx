import React from "react";
import Link from "next/link";

export default function Home() {
  return (
    <main className="flex min-h-screen flex-row justify-center items-center gap-10 p-24">
      <Link href="/exercise1">
        <div className="text-xl md:text-4xl font-bold p-3 rounded-xl bg-orange-400">
          Exercise 1
        </div>
      </Link>

      <Link href="/exercise2">
        <span className="text-xl md:text-4xl font-bold p-3 rounded-xl bg-green-400">
          Exercise 2
        </span>
      </Link>
    </main>
  );
}
