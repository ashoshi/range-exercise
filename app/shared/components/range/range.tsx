import React, { useState, useRef, useEffect, MouseEventHandler } from "react";

import styles from "./range.module.scss";

import { RangeType } from "../../types/range";

const addHandlers = (eventMap: any) => {
  Object.keys(eventMap).forEach((key) => {
    if (typeof document !== "undefined") {
      document.addEventListener(key, eventMap[key], false);
    }
  });
};

const removeHandlers = (eventMap: any) => {
  Object.keys(eventMap).forEach((key) => {
    if (typeof document !== "undefined") {
      document.removeEventListener(key, eventMap[key], false);
    }
  });
};

const usePrevious = (value: any): any => {
  const ref = useRef();
  useEffect(() => {
    ref.current = value;
  }, [value]);
  return ref.current;
};

/**
 * Range component
 * @param values - Range values
 * @returns Range component
 *
 * @example
 * Normal range
 * <Range values={{ min: 0, max: 20 }} />
 *
 * @example
 * Fixed range
 * <Range values={[0, 10, 20, 30, 40, 50]} />
 */
const Range = ({
  values,
  suffix,
  prefix,
  onChange,
}: {
  values: RangeType;
  suffix?: string;
  prefix?: string;
  onChange: (values: number[]) => void;
}) => {
  const rangeRef = useRef<HTMLDivElement>(null);
  const bulletMin = useRef<HTMLDivElement>(null);
  const bulletMax = useRef<HTMLDivElement>(null);

  const step = 1;
  let _startValue = 0;
  let _startPosition = 0;
  let _index = -1;

  let [value, setValue] = useState<number[]>([]);
  let [offset, setOffset] = useState<number[]>([]);
  let [pending, setPending] = useState(false);
  let [index, setIndex] = useState(-1);

  const isFixedRange = Array.isArray(values);
  const fixedValues = isFixedRange ? values : [];

  let [min, max] = isFixedRange
    ? [values[0], values[values.length - 1]]
    : [values.min, values.max];

  let [minRange, setMinRange] = useState(min);
  let [maxRange, setMaxRange] = useState(max);
  const prevOffset = usePrevious(offset);

  let [minText, setMinText] = useState(String(min));
  let [maxText, setMaxText] = useState(String(max));

  let [maxWidth, setMaxWidth] = useState(0);

  let [rangeLength, setRangeLength] = useState(0);

  const pauseEvent = (e: React.MouseEvent) => {
    if (e && e.stopPropagation) {
      e.stopPropagation();
    }
    if (e && e.preventDefault) {
      e.preventDefault();
    }
    return false;
  };

  const getMouseEventMap = () => {
    return {
      mousemove: onMouseMove,
      mouseup: onMouseUp,
    };
  };

  const getValueFromPosition = (position: number) => {
    const diffValue = (position / (rangeLength - 15)) * (max - min);
    return trimAlignValue(_startValue + diffValue);
  };

  const getDiffPosition = (position: number) => {
    let diffPosition = position - _startPosition;
    return diffPosition;
  };

  const onMouseMove = (e: React.MouseEvent) => {
    setPending(true);

    const position = getMousePosition(e);
    const diffPosition = getDiffPosition(position[0]);
    const newValue = getValueFromPosition(diffPosition);

    move(newValue);
  };

  const onMouseUp = (e: React.MouseEvent) => {
    onEnd(getMouseEventMap());
  };

  const onEnd = (eventMap: any) => {
    if (eventMap) {
      removeHandlers(eventMap);
    }

    setPending(false);
    setIndex(-1);
  };

  const formatInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    const input = e.target;
    const name = input.id;

    if (name === "min-range") {
      setMinText(input.value);
    } else {
      setMaxText(input.value);
    }
  };

  const trimAlignValue = (val: number) => {
    return alignValue(trimValue(val));
  };

  const alignValue = (val: number) => {
    const valModStep = (val - min) % step;
    let alignedValue = val - valModStep;

    if (Math.abs(valModStep) * 2 >= step) {
      alignedValue += valModStep > 0 ? step : -step;
    }

    return parseFloat(alignedValue.toFixed(5));
  };

  const trimValue = (val: number) => {
    let trimmed = val;
    if (trimmed <= min) {
      trimmed = min;
    }
    if (trimmed >= max) {
      trimmed = max;
    }

    return trimmed;
  };

  const calcOffset = (value: number) => {
    const range = max - min;

    if (range === 0) {
      return 0;
    }

    return ((value - min) / range) * maxWidth;
  };

  const calcValue = (offset: number) => {
    return (offset / maxWidth) * (max - min) + min;
  };

  const findNearestThumb = (value: number) => {
    const diffMax = Math.abs(value - maxRange);
    const diffMin = Math.abs(value - minRange);

    if (diffMin <= diffMax) {
      setMinRange(value);
    } else {
      setMaxRange(value);
    }
  };

  const calcOffsetFromPosition = (position: number) => {
    if (rangeRef.current) {
      const rangeRect = rangeRef.current.getBoundingClientRect();
      const rangeMax = rangeRect["right"];
      const rangeMin = rangeRect["left"];

      const windowOffset = window[`scrollX`];
      const sliderStart = windowOffset + rangeMin;

      let pixelOffset = position - sliderStart;

      pixelOffset -= 15 / 2;

      return pixelOffset;
    } else return 0;
  };

  const getMousePosition = (e: React.MouseEvent) => {
    return [e["pageX"], e["pageY"]];
  };

  const getTouchPosition = (e: React.TouchEvent) => {
    const touch = e.touches[0];
    return [touch["pageX"], touch["pageY"]];
  };

  const onRangeClick = (e: React.MouseEvent) => {
    const position = getMousePosition(e);
    const valueAtPosition = trimAlignValue(
      calcValue(calcOffsetFromPosition(position[0]))
    );

    findNearestThumb(valueAtPosition);
  };

  const onPressEnter = (
    e: React.KeyboardEvent<HTMLInputElement>,
    name: string
  ) => {
    if (e.key === "Enter") {
      if (name === "min-range") {
        let newValue = Number(minText);

        if (Number.isNaN(newValue)) {
          setMinText(minRange.toString());
        }

        if (newValue < min) {
          setMinText(min.toString());
          setMinRange(min);
        } else if (newValue > maxRange) {
          setMinText(maxRange.toString());
          setMinRange(maxRange);
        } else {
          setMinRange(newValue);
        }
      } else {
        let newValue = Number(maxText);

        if (Number.isNaN(newValue)) {
          setMaxText(maxRange.toString());
        }

        if (newValue > max) {
          setMaxText(max.toString());
          setMaxRange(max);
        } else if (newValue < minRange) {
          setMaxText(minRange.toString());
          setMaxRange(minRange);
        } else {
          setMaxRange(newValue);
        }
      }
    }
  };

  useEffect(() => {
    if (rangeRef.current) {
      const rangeRect = rangeRef.current.getBoundingClientRect();
      const rangeMax = rangeRect["right"];
      const rangeMin = rangeRect["left"];
      const rangeSize = rangeRef.current["clientWidth"];

      setRangeLength(Math.abs(rangeMax - rangeMin));
      setMaxWidth(rangeSize - 15);
    }
  }, []);

  useEffect(() => {
    const tempOffset = [];
    const tempValue = Array.isArray(values) ? values : [min, max];

    tempOffset[0] = calcOffset(tempValue[0]);
    tempOffset[1] = calcOffset(tempValue[tempValue.length - 1]);

    setValue(tempValue);
    setOffset(tempOffset);
  }, [maxWidth]);

  useEffect(() => {
    const newOffsetMin = calcOffset(minRange);
    const newOffsetMax = calcOffset(maxRange);

    setOffset([newOffsetMin, newOffsetMax]);
  }, [minRange, maxRange]);

  useEffect(() => {
    if (prevOffset !== undefined) {
      if (prevOffset !== offset) {
        onChange([minRange, maxRange]);
      }
    }
  }, [offset]);

  const onMouseDown = (i: number) => (e: React.MouseEvent) => {
    if (e.button === 2) {
      return;
    }

    setPending(true);

    const position = getMousePosition(e);
    start(position[0], i);
    addHandlers(getMouseEventMap());
    pauseEvent(e);
  };

  const start = (position: number, i: number) => {
    _startValue = i == 0 ? minRange : maxRange;
    _startPosition = position;
    _index = i;
    setIndex(i);
  };

  const move = (newValue: number) => {
    if (_index == 0) {
      if (newValue > maxRange) {
        return;
      }

      if (isFixedRange) {
        let nearValue = getNearestFixedValue(newValue);
        newValue = nearValue;
      }

      setMinText(newValue.toString());
      setMinRange(newValue);
    } else {
      if (newValue < minRange) {
        return;
      }

      if (isFixedRange) {
        let nearValue = getNearestFixedValue(newValue);
        newValue = nearValue;
      }

      setMaxText(newValue.toString());
      setMaxRange(newValue);
    }
  };

  const getNearestFixedValue = (value: number) => {
    const diffs = fixedValues.map((val) =>
      parseFloat(Math.abs(value - val).toFixed(2))
    );
    const minDiff = Math.min(...diffs);
    return fixedValues[diffs.indexOf(minDiff)];
  };

  const buildBulletStyle = (offset: number, i: number) => {
    const style: any = {
      position: "absolute",
      touchAction: "none",
      zIndex: index == i ? 1 : 0,
      backgroundColor: i == 0 ? "blue" : "red",
      cursor: index == i ? "grabbing" : "grab",
    };
    style["left"] = `${offset}px`;

    return style;
  };

  const buildRootStyle = () => {
    const style = {
      cursor: index == 0 || index == 1 ? "grabbing" : "unset",
    };

    return style;
  };

  return (
    <div className={styles.range_root_container} style={buildRootStyle()}>
      <span className={styles.input_range_container}>
        {prefix ?? ""}
        <input
          id="min-range"
          type="text"
          disabled={isFixedRange}
          name="min-range"
          className={styles.input_range}
          value={minText}
          onChange={formatInput}
          onKeyDown={(e) => onPressEnter(e, "min-range")}
          data-testid="min-input"
        />
        {suffix ?? ""}
      </span>

      <div ref={rangeRef} className={styles.trail}>
        <div
          ref={bulletMin}
          className={styles.bullet}
          style={buildBulletStyle(offset[0], 0)}
          tabIndex={0}
          role="slider"
          onMouseDown={onMouseDown(0)}
        ></div>

        <div
          ref={bulletMax}
          className={styles.bullet}
          style={buildBulletStyle(offset[1], 1)}
          tabIndex={0}
          role="slider"
          onMouseDown={onMouseDown(1)}
        ></div>
      </div>

      <span className={styles.input_range_container}>
        {prefix ?? ""}
        <input
          id="max-range"
          type="text"
          disabled={isFixedRange}
          name="max-range"
          className={styles.input_range}
          value={maxText}
          onChange={formatInput}
          onKeyDown={(e) => onPressEnter(e, "max-range")}
          data-testid="max-input"
        />
        {suffix ?? ""}
      </span>
    </div>
  );
};

export default Range;
