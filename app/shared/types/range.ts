type NormalRange = {
  min: number;
  max: number;
};

type FixedRange = number[];

export type RangeType = NormalRange | FixedRange;
